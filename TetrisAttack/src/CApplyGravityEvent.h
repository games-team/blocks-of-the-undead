#ifndef _CAPPLYGRAVITYEVENT_
#define _CAPPLYGRAVITYEVENT_

#include "CPlayingField.h"
#include "CBlock.h"

class CApplyGravityEvent : public CSequencedEvent
{
public:
	CApplyGravityEvent(CBlock &b, CPlayingField &f) : CSequencedEvent(true), block(b), field(f), pause(0), stopped(true), falling(true) {}

	CApplyGravityEvent* getCopy() { return new CApplyGravityEvent(*this); }

	void doUpdate();
	void initialize();

private:
	CBlock &block;
	CPlayingField &field;
	CTimer pause;
	bool stopped, falling;
};

#endif
