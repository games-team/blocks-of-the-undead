#ifndef _CTETRISTHEME_
#define _CTETRISTHEME_

#include "oodle.h"
#include <string>
using namespace std;

class CTetrisTheme
{
public:
	CTetrisTheme(const string &name = "1", const string &res = "640x480");
	~CTetrisTheme();

	CSurface* getBackground();
	CBlockSet* getBlockSet();
	CSurface* getCursor();
	CSprite* getCharacter();
	CSprite* getFloater();
	CSound& getSwapSound();
	CSound& getHitSound();

private:
	CSurface *background;
	CSurface *cursor;
	CBlockSet *blockSet;
	CSprite *character;
	CSprite *floater;
	CSound swap,hit;
};

#endif