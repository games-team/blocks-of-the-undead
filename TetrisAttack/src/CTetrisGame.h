#ifndef _CTETRISGAME_
#define _CTETRISGAME_

#include "oodle.h"
#include "CPlayingField.h"
#include "CTetrisTheme.h"
#include "CBlockSet.h"

class CTetrisGame : public CSDLGame
{
public:
	CTetrisGame(int screenX, int screenY, int bitDepth, bool fullscreen, CTetrisTheme *theme, const list<CVector> &fieldOffsets);
	CTetrisGame(int screenX, int screenY, int bitDepth, bool fullscreen, const string &title = "");
	~CTetrisGame();

	void setTheme(CTetrisTheme* t, const list<CVector> &fieldOffsets);
	CSprite* addSprite(CVector velocity, CVector pos, const string &filename, CImgLoader::GraphicFileType fileType);
	CSprite* addSprite(CSprite* sprite);
	//CBlock* addBlock(
	enum State
	{
		Splash,
		Loading,
		Win,
		SinglePlayer,
		SinglePlayerPuzzle,
		TwoPlayerHumanVsHuman,
		TwoPlayerHumanVsComp
	};

protected:
	bool gameLoop(bool infinite = true);

private:
	void animateSprites();
	void handleEvent(const SDL_Event &e);
	void handleKeypress(const SDL_KeyboardEvent &e);
	void handleMouseMotion(const SDL_MouseMotionEvent &e);
	void handleMouseButton(const SDL_MouseButtonEvent &e);

	vector<CPlayingField*> fields;
	list<CSprite*> sprites;
	CTetrisTheme* theme;
//	/CSoundSystem sound;
	CSurface *splash;
	State state; 
	bool done;
	CVector mouseloc;
};

#endif