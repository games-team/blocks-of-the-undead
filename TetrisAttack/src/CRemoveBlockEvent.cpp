#include "CRemoveBlockEvent.h"


void CRemoveBlockEvent::doUpdate()
{
	block->clear();
	setConcurrent();
}

CRemoveBlockEvent* CRemoveBlockEvent::getCopy()
{
	return new CRemoveBlockEvent(*this);
}