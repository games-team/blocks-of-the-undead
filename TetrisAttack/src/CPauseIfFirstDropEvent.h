#ifndef _CPAUSEIFFIRSTDROPEVENT_
#define _CPAUSEIFFIRSTDROPEVENT_

#include "CPauseEvent.h"

class CPauseIfFirstDropEvent : public CPauseEvent
{
public:
	CPauseIfFirstDropEvent(int time, bool c = false) : CPauseEvent(time, c) { }
	
	CPauseIfFirstDropEvent* getCopy() { return new CPauseIfFirstDropEvent(*this); }


	void doUpdate()
	{
		if (!d)
		{
			CPauseEvent::doUpdate();
		}
		else
		{
			setDone();
			return;
		}

		CGotoData* d2 = dynamic_cast<CGotoData*>(d);

		if (!d2)
		{
			CPauseEvent::doUpdate();
		}
		else
		{
			setDone();
			return;
		}
	}

};

#endif