#ifndef _CCURSOR_
#define _CCURSOR_

#include "CFieldSprite.h"

//! Class to implement cursor on playing field
class CCursor : public CFieldSprite
{
public:
	CCursor(int x, int y, int fieldWidth, int fieldHeight, CVector offset);
};

#endif