#include <iostream>
#include <sstream>

#include "oodle.h"
// For compilers that support precompilation, includes "wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

using namespace std;

CSimpleGame g(150, 150, 32, false);
CSprite *sprite = NULL;

CColor colors[] = { CColor(), CColor(255,255,255), CColor(255), CColor(0,255), CColor(0,0,255), CColor(255,255) };

class MyApp : public wxApp
{
public:
	virtual bool OnInit();
	
};



class MyFrame : public wxFrame
{
	public:

	MyFrame(const wxString& title, const wxPoint& pos, const wxSize& size);
        virtual ~MyFrame();

	void DoBegin();
	void DoPause();
	void DoStop();

	void OnIdle(wxTimerEvent& WXUNUSED(event));
	void OnQuit(wxCommandEvent& event);
	void OnOpen(wxCommandEvent& event);
	void OnScroll(wxScrollEvent& event);
	void OnPlay(wxCommandEvent& event);
	void OnBegin(wxCommandEvent& event);
	void OnEnd(wxCommandEvent& event);
	void OnPause(wxCommandEvent& event);
	void OnStop(wxCommandEvent& event);
	void OnSpeed(wxCommandEvent& event);
	void OnLoop(wxCommandEvent& event);
	void OnColor(wxCommandEvent& event);

	DECLARE_EVENT_TABLE()

	enum
	{
		ID_Quit = 1,
		ID_Open,
		ID_Controls_Begin,
		ID_Controls_End,
		ID_Controls_Play,
		ID_Controls_Pause,
		ID_Controls_Stop,
		ID_Speed_Text,
		ID_Loop,
		ID_Color,
		ID_TIMER
	};

	wxScrollBar *scroll;
	wxTimer* m_timer;
	wxChoice* loop, *color;
	wxTextCtrl* speedctl;
};

BEGIN_EVENT_TABLE(MyFrame, wxFrame)
	EVT_MENU(ID_Quit, MyFrame::OnQuit)
	EVT_MENU(ID_Open, MyFrame::OnOpen)
	EVT_SCROLL(MyFrame::OnScroll)
	EVT_BUTTON(ID_Controls_Play, MyFrame::OnPlay)
	EVT_TIMER(ID_TIMER, MyFrame::OnIdle)
	EVT_BUTTON(ID_Controls_Begin, MyFrame::OnBegin)
	EVT_BUTTON(ID_Controls_Pause, MyFrame::OnPause)
	EVT_BUTTON(ID_Controls_Stop, MyFrame::OnStop)
	EVT_BUTTON(ID_Controls_End, MyFrame::OnEnd)
	EVT_TEXT(ID_Speed_Text, MyFrame::OnSpeed)
	EVT_CHOICE(ID_Color, MyFrame::OnColor)
	EVT_CHOICE(ID_Loop, MyFrame::OnLoop)
END_EVENT_TABLE()


IMPLEMENT_APP(MyApp)

bool MyApp::OnInit()
{
	//g.addSprite(new CSprite(CVector(0,0), CVector(0,0), "block1.bmp", "static", CRect(0,0,0,0), false) );
	MyFrame *frame = new MyFrame( "Animation Viewer", wxDefaultPosition, wxSize(400,145) );
	frame->Show(TRUE);
	SetTopWindow(frame);
	
	return TRUE;
}
MyFrame::~MyFrame()
{
	delete m_timer;
//	delete scroll;
}

void MyFrame::OnLoop(wxCommandEvent& event)
{
	if (sprite)
	if (event.GetSelection() == 1)
	{
		sprite->getCurrentAnimation()->setLoop(false);
	}
	else
	{
		sprite->getCurrentAnimation()->setLoop(true);
	}
}

void MyFrame::OnColor(wxCommandEvent& event)
{
	g.setBGColor( colors[event.GetSelection()] );

}


void MyFrame::OnSpeed(wxCommandEvent& event)
{
	if (sprite)
	{
		long s = 0;
		speedctl->GetValue().ToLong(&s);
		sprite->getCurrentAnimation()->setSpeed( (int)s );
	}
}

void MyFrame::OnScroll(wxScrollEvent& event)
{
	ostringstream s;
	s << "Frame " << event.GetPosition()+1 << "/" << scroll->GetRange();
	SetStatusText(s.str().c_str());
	if (sprite)
		sprite->getCurrentAnimation()->setFrame(event.GetPosition());
}
void MyFrame::OnIdle(wxTimerEvent& e)
{
	g.update();
	if(sprite)
	{
		scroll->SetThumbPosition(sprite->getCurrentAnimation()->getFrameIndex());
		ostringstream s;
		s << "Frame " << sprite->getCurrentAnimation()->getFrameIndex()+1 << "/" << scroll->GetRange();
		SetStatusText(s.str().c_str());
	}
}
void MyFrame::OnPlay(wxCommandEvent& event)
{
	bool t = m_timer->Start(2);
	if (sprite) sprite->getCurrentAnimation()->start();
}

void MyFrame::OnEnd(wxCommandEvent& event)
{
	sprite->getCurrentAnimation()->setFrame(sprite->getCurrentAnimation()->getFrames()-1);
	DoPause();
	
	//g.update();
	//m_timer->Stop();
}

void MyFrame::OnBegin(wxCommandEvent& event)
{
	DoBegin();
}

void MyFrame::DoBegin()
{
	sprite->getCurrentAnimation()->setFrame(0);
	DoPause();
	//g.update();
	//m_timer->Stop();
}


void MyFrame::OnPause(wxCommandEvent& event)
{
	DoPause();
}

void MyFrame::DoPause()
{
	sprite->getCurrentAnimation()->pause();
}

void MyFrame::OnStop(wxCommandEvent& event)
{
	DoStop();
}

void MyFrame::DoStop()
{
	DoBegin();
	DoPause();
}	

MyFrame::MyFrame(const wxString& title, const wxPoint& pos, const wxSize& size)
: wxFrame((wxFrame *)NULL, -1, title, pos, size)
{
	wxMenu *menuFile = new wxMenu;
	m_timer = new wxTimer(this, ID_TIMER);

	menuFile->Append( ID_Open, "&Open animation..." );
	menuFile->AppendSeparator();
	menuFile->Append( ID_Quit, "E&xit" );

	wxMenuBar *menuBar = new wxMenuBar;
	menuBar->Append( menuFile, "&File" );

	SetMenuBar( menuBar );
	SetStatusBarPane( -1 );
	CreateStatusBar();

	ostringstream s;
	s << "Frame " << 0 << "/" << 0;
	SetStatusText(s.str().c_str());
	
	wxPanel *panel1 = new wxPanel(this, -1);
	/*wxPanel *panel2 = new wxPanel(panel1, -1);
	wxPanel *panel3 = new wxPanel(panel1, -1);*/
	scroll = new wxScrollBar(panel1, -1, wxPoint(0,0), wxSize(-1,-1), wxSB_HORIZONTAL);

	wxBoxSizer *sizer1 = new wxBoxSizer(wxVERTICAL);
	wxBoxSizer *sizer2 = new wxBoxSizer(wxVERTICAL);
	wxBoxSizer *sizer3 = new wxBoxSizer(wxHORIZONTAL);
	wxBoxSizer *sizer4 = new wxBoxSizer(wxHORIZONTAL);

	sizer2->Add( scroll, 1,  wxEXPAND , 4 );
	sizer3->Add( new wxButton(panel1, ID_Controls_Begin, "<", wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT ), 0, wxEXPAND | wxALIGN_CENTER, 0 );
	sizer3->Add( new wxButton(panel1, ID_Controls_Pause, "Pause", wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT ), 0, wxEXPAND | wxALIGN_CENTER, 0 );
	sizer3->Add( new wxButton(panel1, ID_Controls_Play, "Play", wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT ), 0, wxEXPAND | wxALIGN_CENTER, 0 );
	sizer3->Add( new wxButton(panel1, ID_Controls_Stop, "Stop", wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT ), 0, wxEXPAND | wxALIGN_CENTER, 0 );
	sizer3->Add( new wxButton(panel1, ID_Controls_End, ">", wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT ), 0, wxEXPAND | wxALIGN_CENTER, 0 );

	sizer4->Add( new wxStaticText(panel1, -1, "Speed: ", wxDefaultPosition),0, wxALIGN_CENTER_VERTICAL | wxLEFT, 5 );
	speedctl = new wxTextCtrl(panel1, ID_Speed_Text, "30", wxDefaultPosition, wxSize(35, -1));
	sizer4->Add(speedctl , 0, wxALIGN_CENTER, 0);
	sizer4->Add( new wxStaticText(panel1, -1, "Loop: ", wxDefaultPosition),0, wxALIGN_CENTER_VERTICAL | wxLEFT, 5 );
	wxString strings[2] = {"Yes", "No"};
	loop = new wxChoice(panel1, ID_Loop, wxDefaultPosition, wxDefaultSize, 2, strings);
	loop->SetSelection(0);
	sizer4->Add(loop , 0, wxALIGN_CENTER, 0);

	sizer4->Add( new wxStaticText(panel1, -1, "Color: ", wxDefaultPosition),0, wxALIGN_CENTER_VERTICAL | wxLEFT, 5 );
	wxString strings2[6] = {"Black", "White", "Red", "Green", "Blue", "Yellow"};
	color = new wxChoice(panel1, ID_Color, wxDefaultPosition, wxDefaultSize, 6, strings2);
	color->SetSelection(0);
	sizer4->Add(color , 0, wxALIGN_CENTER, 0);
	
	sizer1->Add( sizer3, 0,  wxALIGN_CENTER, 0);
	sizer1->Add( sizer4, 0, wxALIGN_CENTER, 0);
	sizer1->Add( sizer2, 1,wxEXPAND |  wxALIGN_CENTER, 0);

	panel1->SetSizer( sizer1 );

	/*panel1->SetSizer( sizer1 );
	panel1->SetAutoLayout( TRUE );
	sizer1->Fit( panel1 );*/

	/*panel2->SetSizer( sizer2 );
	panel2->SetAutoLayout( TRUE );
	sizer2->Fit( panel2 );*/

	/*panel3->SetSizer( sizer3 );
	panel3->SetAutoLayout( TRUE );
	sizer3->Fit( panel3 );*/

	scroll->SetScrollbar(0, 1, 1, 1, TRUE);
	
		bool t = m_timer->Start(2);

}

void MyFrame::OnQuit(wxCommandEvent& WXUNUSED(event))
{
Close(TRUE);
}


void MyFrame::OnOpen(wxCommandEvent& WXUNUSED(event))
{
	try
	{
		wxFileDialog f(this, "Choose animation file...", "", "", "Image files|*.bmp;*.png", wxOPEN);
		if (f.ShowModal() == wxID_OK)
		{
			wxTextEntryDialog e1(this, "Tile width:", "", "0", wxOK);
			wxTextEntryDialog e2(this, "Tile height:", "", "0", wxOK);
			wxTextEntryDialog e3(this, "Padding:", "", "0", wxOK);
			wxTextEntryDialog e4(this, "Speed:", "", "0", wxOK);

			e1.ShowModal(); e2.ShowModal(); e3.ShowModal(); e4.ShowModal();
			long width = 0;
			e1.GetValue().ToLong(&width);
			long height = 0;
			e2.GetValue().ToLong(&height);
			long padding = 0;
			e3.GetValue().ToLong(&padding);
			long speed = 0;
			e4.GetValue().ToLong(&speed);
			
			speedctl->Clear();
			*speedctl << speed;

			wxString s = f.GetDirectory() + "/" + f.GetFilename();
			g.removeSprite(sprite);
			delete sprite;
			if (width != 0 && height != 0)
			{
				sprite = new CSprite(ZERO_VECTOR, ZERO_VECTOR, s.c_str(), "static", CRect(0,0,0,0), width, height, padding, speed, true, true);
				g.addSprite(sprite);
			}
			else
			{
				sprite = new CSprite(ZERO_VECTOR, ZERO_VECTOR, s.c_str(), "static", CRect(0,0,0,0), true,true);
				g.addSprite(sprite);
			}

			g.resize(sprite->getWidth(), sprite->getHeight(), 32, false);

			SetStatusText("Loaded " + s);
			scroll->SetScrollbar(0, 1, sprite->getCurrentAnimation()->getFrames(), 1, TRUE);
			DoStop();
		}
	}
	catch(ex_Generic e)
	{
		wxMessageBox(e.what());
	}
}

