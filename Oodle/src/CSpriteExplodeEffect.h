#ifndef _CSPRITEEXPLODEEFFECT_
#define _CSPRITEEXPLODEEFFECT_

#include "CExplodeEffect.h"

class CSpriteExplodeEffect : public CExplodeEffect
{
public:
	CSpriteExplodeEffect(CSprite &s, int duration, double minVel, double randVel, const CRect &r, int xPieces, int yPieces, bool concurrent);
	CSpriteExplodeEffect(const CSpriteExplodeEffect &e);

	virtual CExplodeEffect* getCopy();

	virtual void initialize();
	virtual void doUpdate();

private:
	int xPieces, yPieces;
};

#endif
