#ifndef _EXCEPTIONS_
#define _EXCEPTIONS_

#include <stdexcept>
#include <ostream>
#include <string>
#include <queue>
#include "SDL.h"



using namespace std;

//! Class to hold stack tace info for each method that handles an exception
class CTraceInfo
{
public:
	CTraceInfo(const string &c, const string &method, const string &params)
	            : c(c), method(method), params(params)
	{}

	string getClass() const { return c; }
	string getMethod() const { return method; }
	string getParams() const { return params; }
private:
	string c, method, params;
};

//! Generic exception base class
class ex_Generic : public std::runtime_error {
public:
	ex_Generic(const std::string& name, const CTraceInfo &t)
		: std::runtime_error(name), desc("")
	{
		addTraceInfo(t);
	}

	void setDesc(const string& s)
	{
		desc = s;
	}

	void addTraceInfo(const CTraceInfo &t) 
	{
		trace.push(t);  // only problem here is if this throws an exception.  if that happens, the CRT will terminate() the program.  fix this eventually, maybe
	}

	void stackTrace(std::ostream &o)
	{
		if (trace.size() > 0)
		{
			const CTraceInfo t = trace.front(); trace.pop();
			o << "Exception in " << t.getClass() << "::" << t.getMethod() << "(" << t.getParams() << "): " << what();
			if (desc != "")
			{
				o << " (Detailed: " << desc << ")";
			}
			o << endl;
		}
		else
		{
			o << "Ugh!  Something is really fubared.  Cannot print stack trace." << endl;   // haha this should never happen.  if this happens, something is fubared
		}

		while (trace.size() > 1)
		{
			CTraceInfo t = trace.front(); trace.pop();
			o << "\t" << "rethrown at " << t.getClass() << "::" << t.getMethod() << "(" << t.getParams() << ")" << endl;
		}

		if (trace.size() > 0)
		{
			CTraceInfo t = trace.front(); trace.pop();
			o << "\t" << "**caught at " << t.getClass() << "::" << t.getMethod() << "(" << t.getParams() << ")" << endl;
		}
	}

#        ifdef __GNUC__
	         ~ex_Generic() throw () {} // GCC has an error otherwise -- looser throw specifier http://www.agapow.net/index.php?/agapow/content/pdf/248
#        endif

private:
#ifdef __GNUC__
	queue< CTraceInfo > trace; // GCC's libstdc++ apparently won't deal with const objects
#else
	queue< const CTraceInfo > trace;
#endif
	string desc;
};

//! Illegal arguments exception
/*! Thrown when there is some error with arguments passed to method
*/
class ex_illegalArguments : public ex_Generic {
 public:
   ex_illegalArguments(const std::string& name, const CTraceInfo &t)
     : ex_Generic(name, t)
     { }
 };

//! Illegal state exception
/*! Thrown when there the object is not in a legal state for requested action
*/
class ex_illegalState : public ex_Generic {
 public:
   ex_illegalState(const std::string& name, const CTraceInfo &t)
     : ex_Generic(name, t)
     { }
 };

//! Bad Conversion Exception
/*! Thrown when there is some error converting some value
*/
class ex_badConversion : public ex_Generic {
public:
   ex_badConversion(const std::string& name, const CTraceInfo &t)
     : ex_Generic(name, t)
     { }
 };

//! Generic SDL exception base class
/*! Thrown when there is some error with SDL
*/
class ex_sdl : public ex_Generic {
 public:
	 ex_sdl(const std::string& name, const CTraceInfo &t)
     : ex_Generic(name, t)
     {
		const char* sdlerr = SDL_GetError();
		if (sdlerr)
		{
			setDesc(sdlerr);
		}
	 }
 };

//! Generic SDL_Image exception base class
/*! Thrown when there is some error with SDL_Image
*/
class ex_sdlImage : public ex_sdl {
 public:
   ex_sdlImage(const std::string& name, const CTraceInfo &t)
     : ex_sdl(name, t)
     { }
 };

//! SDL Init Failed exception
/*! Thrown when there is some error initializing SDL 
*/
class ex_sdlInitFailed : public ex_sdl {
 public:
	 ex_sdlInitFailed(const std::string& name, const CTraceInfo &t)
     : ex_sdl(name, t)
     { }
 };

//! SDL Surface creation failure
/*! Thrown when there is some error creating SDL surface 
*/
class ex_sdlSurfaceCreationFailed : public ex_sdl {
 public:
   ex_sdlSurfaceCreationFailed(const std::string& name, const CTraceInfo &t)
     : ex_sdl(name, t)
     { }
 };

//! SDL Surface conversion failure
/*! Thrown when there is some error converting SDL surface 
*/
class ex_sdlSurfaceConversionFailed : public ex_sdl {
 public:
   ex_sdlSurfaceConversionFailed(const std::string& name, const CTraceInfo &t)
     : ex_sdl(name, t)
     { }
 };

//! SDL Image Load Failed exception
/*! Thrown when there is some error loading an image 
*/
class ex_sdlImageLoadFailed : public ex_sdlImage {
 public:
   ex_sdlImageLoadFailed(const std::string& name, const CTraceInfo &t)
     : ex_sdlImage(name, t)
     { }
 };


//! Macro used when constructing a stack trace object
#define _THROWEX(ex, name, c, method, params)      { \
                                                    ostringstream ss[4]; \
						                            ss[0] << c;\
											        ss[1] << method;\
											        ss[2] << params;\
													ss[3] << name << " (in " << __FILE__ << " @ line " << __LINE__ << ")";\
						                            throw ex(ss[3].str(), CTraceInfo(ss[0].str(), ss[1].str(), ss[2].str()));\
                                                 }

//! Macros used to add trace info to an exception object
#define _ADDTRACEINFO(obj, c, method, params)    { \
                                                    ostringstream ss[3]; \
						                            ss[0] << c;\
											        ss[1] << method;\
											        ss[2] << params;\
						                            obj.addTraceInfo(CTraceInfo(ss[0].str(), ss[1].str(), ss[2].str()));\
                                                 }

#endif
