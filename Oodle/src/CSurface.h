#ifndef _CSURFACE_
#define _CSURFACE_

#include "SDL.h"
#include "CColor.h"
#include "CVector.h"
#include "CRect.h"
#include "CGfxPrimitives.h"
#include <list>
using namespace std;
// TODO: Abstract the surface class, use base class CSurface and derived class CSDLSurface.  This will make it easy to move away from SDL if necessary (or optimize stuff).


class CSurface
{

	friend class CGfxPrimitives;

public:
	enum SurfaceTypes
	{
		SOFTWARE_SURFACE = SDL_SWSURFACE,
		HARDWARE_SURFACE = SDL_HWSURFACE,
		SRC_COLORKEY     = SDL_SRCCOLORKEY,
		SRC_ALPHA        = SDL_SRCALPHA
	};
	
	//! Constructs surface with given paramaters
	/*! These are simply passed to SDL_CreateRGBSurface()
	    \exception ex_sdlSurfaceCreationFailed { Call to SDL_CreateRGBSurface() failed }
	*/
	CSurface(Uint32 flags, int width, int height, int depth,
			 Uint32 Rmask, Uint32 Gmask, Uint32 Bmask, Uint32 Amask);

	//! Copy constructor
	CSurface(const CSurface& surf);

	//! Construct CSurface from pre-existing surface
	/*! 'Ownership' of the surface specified by s is transfered this CSurface
	    \param s Pointer to surface that this CSurface will assimilate
	    
		\exception ex_illegalArguments { s is NULL pointer }
	*/
	CSurface(SDL_Surface* s);

	//! Destructor
	/*! Calls SDL_FreeSurface() on s
	*/
	~CSurface();

	//void blit(const CSurface& src, int srcx, int srcy, int srcwidth, int srcheight);
	void blit(const CSurface &src, int x, int y);
	void blitSrcRect(const CSurface& src, CRect r);
	void blitScale(const CSurface& src, CRect r, double scale);
	void blitScaleEx(const CSurface& src, CRect r, double scaleX, double scaleY);
	void blitRotScale(const CSurface& src, CRect r, double scale, double rad);
	void blitBothRect(const CSurface& src, CRect rsrc, CRect rdest);
	void rotateAndScale(double scale, double rad);
	void fill(CColor color);
	void doneDrawing();
	void setAlpha(Uint32 flags, Uint8 alpha);

	//! Gets a section of this surface
	/*! Gets a section of this surface and returns a new CSurface
	    \param x x cord of top-left corner of section to get
		\param y y cord of top-left corner of section to get
		\param width width of section to get
		\param height height of section to get
		\exeption ex_sdlSurfaceCreationFailed { Call to SDL_CreateRGBSurface() failed }
	*/
	CSurface* getSection(int x, int y, int width, int height) const;
	void setClippingRect(CRect r);
	void resetClippingRect();
	void displayFormat();
	void displayFormatAlpha();
	int getWidth() const;
	int getHeight() const;
//	CColor getColor(int r, int g, int b);

	const CSurface& operator=(const CSurface &s);

	bool onSurface(const CVector &pos) const;
	bool onSurface(int x, int y) const;

	void lock();
	void unlock();
	void getPixel(int x, int y, CColor &color);
	void setPixel(int x, int y, CColor &color);

	friend ostream& operator<<(ostream& os, const CSurface& v);   // similar to toString in java

private:

	SDL_Surface* createSurface(Uint32 flags, int width, int height, int depth, 
	                           Uint32 Rmask, Uint32 Gmask, Uint32 Bmask, Uint32 Amask);
	SDL_Surface* getSurface() const;
	
	Uint32 getPixel(int x, int y);
	void setPixel(int x, int y, Uint32 val);

	int width, height;
	int depth;
	Uint32 Rmask, Gmask, Bmask, Amask;
	Uint32 creationFlags;
	SDL_Surface* s;
	int locked;
	list<CRect> dirty;
};

#endif
