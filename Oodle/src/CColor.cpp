#include "CColor.h"
#include "SDL.h"

CColor COLOR_BLACK = CColor(0,0,0);
CColor COLOR_WHITE = CColor(0xff,0xff,0xff);
CColor COLOR_TRANS = CColor(0xff,0xff,0xff,0xff);
CColor COLOR_RED   = CColor(0xff, 0, 0);
CColor COLOR_GREEN = CColor(0, 0xff, 0);
CColor COLOR_BLUE  = CColor(0, 0, 0xff);

void CColor::setR(Uint8 r)
{
	CColor::r = r;
	sdlcolor = SDL_MapRGBA(SDL_GetVideoSurface()->format, r, g, b, a);
}

void CColor::setG(Uint8 g)
{
    this->g = g;
	sdlcolor = SDL_MapRGBA(SDL_GetVideoSurface()->format, r, g, b, a);
}

void CColor::setB(Uint8 b)
{
    this->b = b;
	sdlcolor = SDL_MapRGBA(SDL_GetVideoSurface()->format, r, g, b, a);
}

void CColor::setA(Uint8 a)
{
    this->a = a;
	sdlcolor = SDL_MapRGBA(SDL_GetVideoSurface()->format, r, g, b, a);
}


SDL_Color CColor::getSDLColor()
{
	SDL_Color s = {r,g,b,a};
	return ( s );
}

Uint32 CColor::getDispFormatColor()
{
	if(sdlcolor == 0) sdlcolor = SDL_MapRGBA(SDL_GetVideoSurface()->format, r, g, b, a);
	return sdlcolor;
}