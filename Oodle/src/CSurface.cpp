#include "oodle.h"

CSurface::CSurface(Uint32 flags, int width, int height, int depth, 
				   Uint32 Rmask, Uint32 Gmask, Uint32 Bmask, Uint32 Amask)
				   : width(width), height(height), depth(depth), creationFlags(flags), 
				     Rmask(Rmask), Gmask(Gmask), Bmask(Bmask), Amask(Amask),
				     locked(0)
{
	/*LOG("Creating surface {" << this << "}", 6, LOG_INFO);*/
	s = createSurface(flags, width, height, depth,
	                  Rmask, Gmask, Bmask, Amask);
	if (!s)
	{
		// surface creation failed, throw exception
		_THROWEX(ex_sdlSurfaceCreationFailed, "SDL Surface Creation Failed", "CSurface", "CSurface", 
			"flags = " << flags << ", width = " << width << ", height = " << height << ", depth = "
			<< depth << ", Rmask = " << Rmask << ", Gmask = " << Gmask << ", Bmask = " << Bmask 
			<< ", Amask = " << Amask);
	}

//	displayFormatAlpha();
}

CSurface::CSurface(SDL_Surface* s)
    : s(s), locked(0)
{
	if (!s)
	{
		_THROWEX(ex_illegalArguments, "Illegal argument 's'", "CSurface", "CSurface", "s = " << s);
	}
	width = s->w;
	height = s->h;
	depth = s->format->BitsPerPixel;
	creationFlags = s->flags;
	Rmask = s->format->Rmask;
	Gmask = s->format->Gmask;
	Bmask = s->format->Bmask;
	Amask = s->format->Amask;

//	displayFormatAlpha();
}

CSurface::CSurface(const CSurface &surf)
				   : width(surf.width), height(surf.height), depth(surf.depth), creationFlags(surf.creationFlags), 
				     Rmask(surf.Rmask), Gmask(surf.Gmask), Bmask(surf.Bmask), Amask(surf.Amask),
				     locked(0)
{
	s = SDL_ConvertSurface(surf.s, surf.s->format, surf.s->flags);

	if (!s)
	{
		// surface creation failed, throw exception
		_THROWEX(ex_sdlSurfaceCreationFailed, "SDL Surface Creation Failed", "CSurface", "CSurface", "surf = " << surf);
	}
}

CSurface::~CSurface()
{
	//LOG("Freeing surface {" << this << "}", 6, LOG_INFO);
	SDL_FreeSurface(s);
}

CSurface* CSurface::getSection(int x, int y, int width, int height) const
{
	CSurface* tmp = new CSurface(creationFlags, width, height, depth, Rmask, Gmask, Bmask, Amask);
	
	tmp->blitSrcRect(*this, CRect(x, y, width, height));
	return tmp;
}

void CSurface::blitSrcRect(const CSurface& src, CRect r)
{
	SDL_Rect sr;
	sr.x = (Sint16)r.getX();
	sr.y = (Sint16)r.getY();
	sr.w = (Sint16)r.getWidth();
	sr.h = (Sint16)r.getHeight();
	
	int ret = SDL_BlitSurface(src.s, &sr, s, NULL);
}

void CSurface::blitScale(const CSurface& src, CRect r, double scale)
{
	Uint8 alpha = src.getSurface()->format->alpha;
	SDL_Rect sr;
	sr.x = (Sint16)r.getX();
	sr.y = (Sint16)r.getY();
	
	SDL_Surface* z = zoomSurface(src.s, scale, scale, 1);
	if (z)
	{
		if (alpha != SDL_ALPHA_OPAQUE) 
		{
			SDL_SetAlpha(z, SDL_SRCALPHA, alpha);
		}
		SDL_BlitSurface(z, NULL, s, &sr);
		SDL_FreeSurface(z);
	}
	else
	{
		_THROWEX(ex_sdlSurfaceCreationFailed, "Failed to rotozoom surface", "CSurface", "blitScale", "src = {" << &src << "}, r = {" << &r << "}, scale = " << scale);
	}
//	dirty.push_back(CRect(sr.x,sr.y,z->w, z->h));
}

void CSurface::blitScaleEx(const CSurface& src, CRect r, double scaleX, double scaleY)
{
	Uint8 alpha = s->format->alpha;
	SDL_Rect sr;
	sr.x = (Sint16)r.getX();
	sr.y = (Sint16)r.getY();
	
	SDL_Surface* z = zoomSurface(src.s, scaleX, scaleY, 1);
 	if (z)
	{
		if (alpha != SDL_ALPHA_OPAQUE) SDL_SetAlpha(z, SDL_SRCALPHA, alpha);
		SDL_BlitSurface(z, NULL, s, &sr);
		SDL_FreeSurface(z);
	}
	else
	{
		_THROWEX(ex_sdlSurfaceCreationFailed, "Failed to rotozoom surface", "CSurface", "blitScale", "src = {" << &src << "}, r = {" << &r << "}, scaleX = " << scaleX << ", scaleY = " << scaleY);
	}
//	dirty.push_back(CRect(sr.x, sr.y, z->w, z->h));
}

void CSurface::rotateAndScale(double scale, double rad)
{
	Uint8 alpha = s->format->alpha;
	SDL_Surface* tmp = rotozoomSurface (s, rad * (180/PI), scale, 1);
	if (alpha != SDL_ALPHA_OPAQUE) SDL_SetAlpha(tmp, SDL_SRCALPHA, alpha);
	
	if (!tmp)
		_THROWEX( ex_sdlSurfaceCreationFailed, "Failed to rotozoom surface", "CSurface", "rotateAndScale", "scale=" << scale << ", rad=" << rad);
	
	SDL_FreeSurface(s);
	s = tmp;
}

void CSurface::blitRotScale(const CSurface& src, CRect r, double scale, double rad)
{
	Uint8 alpha = s->format->alpha;
	SDL_Rect sr;
	sr.x = (Sint16)r.getX();
	sr.y = (Sint16)r.getY();
	
	SDL_Surface* z = rotozoomSurface (src.s, rad * (180/PI), scale, 1);
	if (z)
	{
		if (alpha != SDL_ALPHA_OPAQUE) SDL_SetAlpha(z, SDL_SRCALPHA, alpha);
		SDL_BlitSurface(z, NULL, s, &sr);
		SDL_FreeSurface(z);
	}
	else
	{
		_THROWEX(ex_sdlSurfaceCreationFailed, "Failed to rotozoom surface", "CSurface", "blitScale", "src = {" << &src << "}, r = {" << &r << "}, scale = " << scale << ", rad = " << rad);
	}
//	dirty.push_back(CRect(sr.x, sr.y, z->w, z->h));
}

void CSurface::blitBothRect(const CSurface& src, CRect rsrc, CRect rdest)
{
	SDL_Rect sr1;
	sr1.x = (Sint16)rsrc.getX();
	sr1.y = (Sint16)rsrc.getY();
	sr1.w = (Sint16)rsrc.getWidth();
	sr1.h = (Sint16)rsrc.getHeight();

	SDL_Rect sr2;
	sr2.x = (Sint16)rdest.getX();
	sr2.y = (Sint16)rdest.getY();
	sr2.w = (Sint16)rdest.getWidth();
	sr2.h = (Sint16)rdest.getHeight();

	int ret = SDL_BlitSurface(src.s, &sr1, s, &sr2);
//	dirty.push_back(rdest);
}

void CSurface::blit(const CSurface &src, int x, int y)
{
	SDL_Rect r;
	r.x = x;
	r.y = y;
	int b = SDL_BlitSurface(src.s, NULL, s, &r);
/*	if (src.width != width && src.height != height)
		dirty.push_back(CRect(x,y,src.width,src.height));*/
}

void CSurface::fill(CColor color)
{
	//Uint32 surfcolor = SDL_MapRGB(s->format, color.getR(), color.getG(), color.getB());
	SDL_FillRect(s, NULL, color.getDispFormatColor());
	//dirty.clear();
	//dirty.push_back(CRect(0,0,width,height));
}

void CSurface::doneDrawing()
{
	if ((s->flags & SDL_DOUBLEBUF) == SDL_DOUBLEBUF)     // if surface is dbl buffered, we have to flip it
	{
		SDL_Flip(s);
	}
	else
	{
		SDL_UpdateRect(s, 0, 0, 0, 0);   // invalidate/redraw whole screen
	}
	/*for (list<CRect>::iterator i = dirty.begin(); i != dirty.end(); ++i)
	{
		CRect r = (*i);
		SDL_Rect r1 = {r.getX(), r.getY(), r.getWidth(), r.getHeight()};
		//SDL_FillRect(s, &r1, COLOR_BLACK.getDispFormatColor());
		SDL_UpdateRect(s, r.getX(), r.getY(), r.getWidth(), r.getHeight());
	}*/
}

void CSurface::setAlpha(Uint32 flags, Uint8 alpha)
{
	SDL_SetAlpha(s, flags, alpha);
}

const CSurface& CSurface::operator=(const CSurface &s)
{
	if (&s != this)   // don't allow assignment to self
	{
		SDL_FreeSurface(CSurface::s);

		width = s.width;
		height = s.height;
		depth = s.depth;
		creationFlags = s.creationFlags;
        Rmask = s.Rmask;
		Gmask = s.Gmask;
		Bmask = s.Bmask;
		Amask = s.Amask;
		CSurface::s = SDL_ConvertSurface(s.s, s.s->format, s.s->flags);

		if (!CSurface::s)
		{
			_THROWEX(ex_sdlSurfaceCreationFailed, "SDL Surface Creation Failed", "CSurface", "operator=", "s = " << s);
		}
	}
	return *this;
}

SDL_Surface* CSurface::getSurface() const
{
	return s;
}

void CSurface::displayFormat()
{
	SDL_Surface* tmp;
	if (SDL_GetVideoSurface() != NULL && s != SDL_GetVideoSurface())
	{
		tmp = SDL_DisplayFormat(s);
		if (!tmp)
		{
			// throw exception
			_THROWEX(ex_sdlSurfaceConversionFailed, "SDL Surface Creation Failed", "CSurface", "displayFormat", "");
		}
		SDL_FreeSurface(s);
		s = tmp;
	}
}

void CSurface::displayFormatAlpha()
{
	SDL_Surface* tmp;
	if (SDL_GetVideoSurface() != NULL && s != SDL_GetVideoSurface())
	{
		tmp = SDL_DisplayFormatAlpha(s);
		if (!tmp)
		{
			// throw exception
			_THROWEX(ex_sdlSurfaceConversionFailed, "SDL Surface Creation Failed", "CSurface", "displayFormatAlpha", "");
		}
		SDL_FreeSurface(s);
		s = tmp;
	}
}

int CSurface::getWidth() const
{
	return width;
}

int CSurface::getHeight() const
{
	return height;
}

bool CSurface::onSurface(const CVector &pos) const
{
	return onSurface((int)pos.getX(), (int)pos.getY());
}

bool CSurface::onSurface(int x, int y) const
{
	if (x < 0 || x >= width || y < 0 || y >= height)
	{
		return false;
	}
	else
	{
		return true;
	}
}

void CSurface::lock()
{
	// attempt to lock surface if necessary
	if (SDL_MUSTLOCK(s) && locked == 0)
	{
		//cout << "Locking {" << s << "}" << endl;
		if (SDL_LockSurface(s) < 0)
		{
			// FIXME: throw an exception
			return;
		}
		locked++;
	}
}

void CSurface::unlock()
{
	if (locked <= 0)
	{
		// FIXME: throw an exception
		return;
	}

	//cout << "Unlocking {" << s << "}" << endl;
	// unlock surface if necessary
	if (locked > 0)
	{
		SDL_UnlockSurface(s);

		locked--;
	}
}

void CSurface::getPixel(int x, int y, CColor &color)
{
	Uint8 red, green, blue;

	try
	{
		SDL_GetRGB(getPixel(x, y), s->format, &red, &green, &blue);

		color.setR(red);
		color.setG(green);
		color.setB(blue);
	}
	catch (ex_Generic e)
	{
		_ADDTRACEINFO(e, "CSurface", "getPixel",
			      "x = " << x << ", y = " << y << ", color = " << (void*) &color);
		throw e;
	}

}

void CSurface::setPixel(int x, int y, CColor &color)
{
	try
	{
		setPixel(x, y, color.getDispFormatColor());
	}
	catch (ex_Generic e)
	{
		_ADDTRACEINFO(e, "CSurface", "setPixel",
			      "x = " << x << ", y = " << y << ", color = " << (void*) &color);
		throw e;
	}
}

SDL_Surface* CSurface::createSurface(Uint32 flags, int width, int height, int depth, 
                                     Uint32 Rmask, Uint32 Gmask, Uint32 Bmask, Uint32 Amask)
{
	return SDL_CreateRGBSurface(flags, width, height, depth,
	                            Rmask, Gmask, Bmask, Amask);
}

Uint32 CSurface::getPixel(int x, int y)
{
	lock();
	
	if (! onSurface(x, y))
	{
		// the x coord is out of range
		_THROWEX(ex_illegalArguments,
			 "(x, y) does not lie on surface",
			 "CSurface", "getPixel",
			 "x = " << x << ", y = " << y);
	}

	Uint8 *ptr = (Uint8*) s->pixels + y * s->pitch + x * s->format->BytesPerPixel;

	switch (s->format->BytesPerPixel)
	{
	case 1:
		return (Uint32) *ptr;
	case 2:
		return (Uint32) *((Uint16*) ptr);
	case 3:
		if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
			return (Uint32) ptr[0] << 16 | (Uint32) ptr[1] << 8 | (Uint32) ptr[2];
		else
			return (Uint32) ptr[0] | (Uint32) ptr[1] << 8 | (Uint32) ptr[2] << 16;
	case 4:
		return *((Uint32*) ptr);
	default:
		// throw an exception?  this shouldn't EVER happen.
		return 0;
	}
	unlock();
}

void CSurface::setPixel(int x, int y, Uint32 val)
{
	lock();

	if (! onSurface(x, y))
	{
                // the x coord is out of range
		_THROWEX(ex_illegalArguments,
			 "(x, y) does not lie on surface",
			 "CSurface", "setPixel",
			 "x = " << x << ", y = " << y << ", val = " << val);
	}

	Uint8 *ptr = (Uint8*) s->pixels + y * s->pitch + x * s->format->BytesPerPixel;
	
	switch(s->format->BytesPerPixel)
	{
	case 1:
		*ptr = (Uint8) val;
		break;
	case 2:
		*((Uint16*) ptr) = (Uint16) val;
		break;
	case 3:
		if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
		{
			ptr[0] = (Uint8) (val >> 16) & 0xFF;
			ptr[1] = (Uint8) (val >> 8) & 0xFF;
			ptr[2] = (Uint8) val & 0xFF;
		}
		else
		{
			ptr[0] = (Uint8) val & 0xFF;
			ptr[1] = (Uint8) (val >> 8) & 0xFF;
			ptr[2] = (Uint8) (val >> 16) & 0xFF;
		}
		break;
	case 4:
		*((Uint32*) ptr) = val;
		break;
	default:
		break;
	}

	unlock();
}

void CSurface::setClippingRect(CRect r)
{
	SDL_Rect rect = {(Sint16)r.getX(), (Sint16)r.getY(), (Sint16)r.getWidth(), (Sint16)r.getHeight()};
	SDL_SetClipRect(s, &rect);
}

void CSurface::resetClippingRect()
{
	SDL_SetClipRect(s, NULL);
}

ostream& operator<<(ostream& os, const CSurface& v)
{

	return (os << "CSurface @ {" << &v << "}: [width = " << v.width << ", height = " << v.height << ", depth = " << v.depth << 
		", Rmask = " << v.Rmask << ", v.Gmask = " << v.Gmask << ", Bmask = " << v.Bmask << 
		", Amask = " << v.Amask << ", creationFlags = " << v.creationFlags <<
		", locked = " << v.locked << "]");
}
