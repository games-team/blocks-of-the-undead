#ifndef _CTIMEDEVENT_
#define _CTIMEDEVENT_

#include "CSequencedEvent.h"
#include "CTimer.h"

class CTimedEvent : public CSequencedEvent
{
public:
	CTimedEvent(int period, bool c) : CSequencedEvent(c), timer(0), period(period) {}

	bool updateTime() { return timer.isRinging(); }
	unsigned int ticksElapsed() { return timer.getElapsed(); }

	void initialize() { CSequencedEvent::initialize(); timer.setPeriod(period); timer.reset();  }
protected:
	CTimer timer;
	int period;
};

#endif