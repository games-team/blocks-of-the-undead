#ifndef _CVECTOR_
#define _CVECTOR_

#include <iostream>
#include <cmath>
using namespace std;

#define PI		3.14159

class CVector
{
private:
	double x;
	double y;
	
	CVector add(const CVector &b) const;
	CVector subtract(const CVector &b) const;
	CVector multiply(double d) const;
	double dot(const CVector &b) const;

public:
	CVector(double x, double y);
//	CVector(double mag, double rad);

	CVector() : x(0), y(0) { }
	~CVector() {}

	CVector operator+=(CVector b);
	CVector operator+(const CVector &b) const;
	CVector operator-(const CVector &b) const;
	double operator*(const CVector &b) const;		// dot product
	CVector operator*(double d) const;		// scalar product
//	const CVector& operator=(CVector b);
    bool operator==(const CVector &b) const;
    bool operator>(const CVector &b) const;
    bool operator<(const CVector &b) const;
    bool operator>=(const CVector &b) const;
    bool operator<=(const CVector &b) const;
	bool operator!=(const CVector &b) const;
	friend ostream& operator<<(ostream& os, const CVector& v);

	double getDirRad() const { return atan2(y,x); }
	double getDirDeg() const { return getDirRad() * (180 / PI); }
	double getX() const { return x; }
	double getY() const { return y; }
	double getMag() const { return ( sqrt (pow(x,2.0) + pow(y,2.0) ) ); }

	void setX(double x) { CVector::x = x; }
	void setY(double y) { CVector::y = y; }
	void addX(double x) { CVector::x += x; }
	void addY(double y) { CVector::y += y; }
	void setMag(double mag) { double rad = getDirRad(); y = sin(rad)*mag; x = cos(rad)*mag; }
	void setDir(double rad) { double mag = getMag(); y = sin(rad)*mag; x = cos(rad)*mag; }
};

extern CVector ZERO_VECTOR;

#endif
