#ifndef _CPIXELEXPLODEEFFECT_
#define _CPIXELEXPLODEEFFECT_

#include "CExplodeEffect.h"

class CPixelExplodeEffect : public CExplodeEffect
{
public:
	CPixelExplodeEffect(int duration, bool concurrent, CRect r, CSprite &s);
	CPixelExplodeEffect(CExplodeEffect &e);

	virtual CExplodeEffect* getCopy();

	virtual void initialize();
	virtual void doUpdate();	
};

#endif
