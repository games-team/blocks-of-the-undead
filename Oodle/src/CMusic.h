#ifndef _CMUSIC_
#define _CMUSIC_

#include "CSoundSystem.h"
#include "SDL_mixer.h"

class CMusic
{
	friend class CSoundSystem;
public:
	CMusic(const string &snd);

private:
	Mix_Music* data;
};

#endif
	
