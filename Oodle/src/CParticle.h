#ifndef _CPARTICLE_
#define _CPARTICLE_

#include <list>
#include "CRect.h"
#include "CVector.h"
#include "CSurface.h"
#include "CSequencedEvent.h"
#include "CTimer.h"

//! Base class for things that have a position and velocity
class CParticle
{
public:
	enum BoundaryInfo {
		OUTSIDE_LOW = -1,
		INSIDE = 0,
		OUTSIDE_HIGH = 1
	};

	CParticle(CVector v, CVector pos);
	CParticle(CVector v, CVector pos, const CRect &r);
	CParticle(const CParticle &p);

	virtual ~CParticle();

	virtual void update();
	virtual void draw(CSurface &surface) = 0;
	virtual void clear();

	BoundaryInfo inBoundaryHoriz();
	virtual BoundaryInfo inBoundaryHoriz(double x);
	BoundaryInfo inBoundaryVert();
	virtual BoundaryInfo inBoundaryVert(double y);
	void stayInBoundary();

	void setVelocity(CVector vel) { velocity = vel; }
	void changeVelocity(double x, double y) { velocity = CVector(velocity.getX() + x, velocity.getY() + y); }
	void setPos(CVector pos);
	void setPos(double x, double y);
	void setPosRel(double x, double y);
	void setRect(CRect box) { CParticle::box = box; }
	CRect getRect() { return box; }
	void appendEvent(CSequencedEvent* e);
	void appendBehavior(CSequencedEvent* e);
	void prependEvent(CSequencedEvent* e);
	bool hasEvent() { return events.size() > 0; }
	void removeEvent(CSequencedEvent *e) { events.remove(e); }
	void move(double x, double y) { pos.addX(x); pos.addY(y); }
	bool isAlive() { return !dead; }                                   //! Is this particle alive?

	double getX() { return pos.getX(); }
	double getY() { return pos.getY(); }
	CVector getVel() { return velocity; }
	CVector getPos() { return pos; }

protected:
	CVector velocity;                  //! Vector that describes the particle's motion (in pixels per second)
	CVector pos;                       //! Vector that describes the particle's location (origin at top-left of screen)
	bool moving;                       //! If true, the particle is moving (ie its velocity is applied every frame.
	CTimer timer;                      //! timer object to make framerate-independent movement possible
	CRect box;                         //! Particle will stay within this box
	CSequencedEvent::Container events; //! List holding events to execute on this particle
	CSequencedEvent::Container behaviors; //! List holding behaviors to execute on this particle provided that no events are non-concurrent

private:
	bool dead;                         //! particle is dead, should be deleted
};

#endif
