#ifndef _CGOTOANIMCHANGEEVENT_
#define _CGOTOANIMCHANGEEVENT_

#include "CSprite.h"
#include "CSequencedEvent.h"

class CGotoAnimChangeEvent : public CSequencedEvent
{
public:
	CGotoAnimChangeEvent(CSprite &b) : CSequencedEvent(false), b(b) {}
	
	CGotoAnimChangeEvent* getCopy() { return new CGotoAnimChangeEvent(*this); }
	void doUpdate();

private:
	CSprite &b;
};

#endif