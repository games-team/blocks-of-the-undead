#ifndef _CTRANSITION_
#define _CTRANSITION_

#include "CSurface.h"

class CTransition
{
public:
	virtual void update() = 0;
	virtual void draw(CSurface &dest) = 0;
	virtual bool done() = 0;
};

#endif
	