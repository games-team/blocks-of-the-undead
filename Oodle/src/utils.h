#ifndef _UTILS_
#define _UTILS_

#include <exception>
#include <stdexcept>
#include <sstream>
#include <fstream>
#include <string>
#include <time.h>
#include "exceptions.h"
#include "SDL.h"
using namespace std;

//! Enum to describe different types of things that can be written to a log
enum logTypes
{
	LOG_INFO,
	LOG_ERROR
};

//! Function to convert a double to a string
std::string stringify(double x);

//! Function to convert an int to a string
std::string stringify(int x);

//! Function to log output to a file (log.txt) in program's directory
void log(const std::ostringstream &s, int lvl, logTypes type);

//! Macro used when calling log
#define LOG(t, lvl, type)  { \
                        ostringstream ss; \
						ss << t;\
						log(ss, lvl, type);\
                      }

Uint32 getPixel32(SDL_Surface *, int, int);
void setPixel32(SDL_Surface *, int, int, Uint32);

#ifndef __GNUC__
template <class T> void deleteContainer(T& a)
{
	for(T::iterator i = a.begin(); i != a.end(); ++i)
	{
		delete (*i);
	}
}
#endif

#define FOREACH(container, ident, iter, act)   { \
	for (container::iterator iter = ident.begin(); iter != ident.end(); ++iter) { \
	    act; \
	}\
}

#define FOREACH_CONST(container, ident, iter, act)   { \
	for (container::const_iterator iter = ident.begin(); iter != ident.end(); ++iter) { \
	    act; \
	}\
}

extern const Uint32 RMASK, GMASK, BMASK, AMASK;


#endif
