#include "oodle.h"

CSoundSystem::CSoundSystem(int chunksize, int frequency, int channels, Uint16 format)
{
	SDL_InitSubSystem(SDL_INIT_AUDIO);
	if (Mix_OpenAudio(frequency, format, channels, chunksize))
	{
		_THROWEX(ex_sdl, "Failed to load sound subsystem", "CSoundSystem", "CSoundSystem",
			"frequency = " << frequency << ", chunksize = " << chunksize << ", " <<
			"channels = " << channels << ", format = " << format);
	}
}

void CSoundSystem::playSound(CSound &snd) const
{
	snd.setChannel( Mix_PlayChannel(-1, snd.getChunk(), 0) );
}
void CSoundSystem::stopSound(const CSound &snd) const
{
	Mix_HaltChannel(snd.getChannel());
}

CSoundSystem::~CSoundSystem()
{
	Mix_CloseAudio();
}