#ifndef _CTEXT_
#define _CTEXT_

#include <string>
#include "SDL_ttf.h"
using namespace std;

//! Class to generate a sprite from given input text
class CText : public CSprite
{
public:
	enum FontStyle
	{
		NONE,
		BOLD = TTF_STYLE_BOLD,
		ITALIC = TTF_STYLE_ITALIC,
		UNDERLINE = TTF_STYLE_UNDERLINE
	};
	
	CText(const string &txt, CColor c, const string &font, int ptsize, FontStyle s = NONE)
		: CSprite(ZERO_VECTOR, ZERO_VECTOR), ttfont(NULL), color(c)
	{
		if ( TTF_Init() < 0 )
		{
			_THROWEX(ex_sdl, "Failed to initialize the SDL_TTF library", "CText", "CText",
				"txt = '" << txt << "', font = '" << font << "', s = " << s)
		}

		int renderstyle = static_cast <int> (s);

		// Open the font file with the requested point size
		ttfont = TTF_OpenFont(font.c_str(), ptsize);
		if ( ttfont == NULL ) {
			TTF_Quit();
			_THROWEX(ex_sdl, "Failed to open font", "CText", "CText",
				"txt = '" << txt << "', font = '" << font << "', s = " << s)
		}
		setText(txt, s);
	}

	void setText(const string &txt, FontStyle s = NONE)
	{
		setText(txt, s, color);
	}

	void setColor(CColor c)
	{
		color = c;
	}

	void setText(const string &txt, FontStyle s, CColor c)
	{
		int renderstyle = static_cast <int> (s);

		TTF_SetFontStyle(ttfont, renderstyle);

		CSurface *tmp = NULL;
		try
		{
			tmp = new CSurface( TTF_RenderText_Blended(ttfont, txt.c_str(), c.getSDLColor()) );
		}
		catch (ex_illegalArguments e)
		{
			delete tmp;
			TTF_CloseFont(ttfont);
			TTF_Quit();
			_THROWEX(ex_sdl, "Failed to render text to surface", "CText", "CText", 
			          "txt = '" << txt << "', s = " << s);
		}
		tmp->displayFormatAlpha();
		addAnimation("static", new CAnimation(tmp));
		setAnimation("static");
	}

	~CText()
	{
		TTF_CloseFont(ttfont);
		TTF_Quit();
	}

private:
	TTF_Font* ttfont;
	CColor color;
};

#endif