#include "oodle.h"

CSprite::CSprite(CVector v, CVector p, const string &fileName, const string &animName, const CRect& b, bool alpha, bool nopath)
				 : CParticle(v, p, b), currentAnimation(NULL), isClone(false), effect(NULL), scale(1), rot(0)
{
//	try
//	{
		currentAnimation = loadAnimation(fileName, animName, alpha, nopath);
//	}
}

CSprite::CSprite(CVector v, CVector p, const string &fileName, const string &animName, const CRect& b,
				 int width, int height, int padding, int speed, bool alpha, bool nopath)
				 : CParticle(v, p, b), currentAnimation(NULL), isClone(false), effect(NULL), scale(1), rot(0)
{
//	try
//	{
		currentAnimation = loadAnimation(fileName, animName, width, height, padding, speed, alpha, nopath);
//	}
}

CSprite::CSprite(const CSprite &surf)
				   : CParticle(surf),
				     isClone(surf.isClone), effect(NULL), scale(surf.scale), rot(surf.rot)
{
	// copy over all the CAnimation objects (need to make copies, not just copy pointers)
	for (map<string,CAnimation*>::const_iterator i = surf.animations.begin(); i != surf.animations.end(); ++i)
	{
		CAnimation* anim = new CAnimation( *i->second );
		animations[i->first] = anim;
		if (surf.currentAnimation == i->second)   // if this was the current animation of surf, then make the current animation of this point to the newly allocated object
		{
			currentAnimation = anim;
		}
	}
}

CSprite::CSprite(const CSprite& sprite, bool clone)
				   : CParticle(sprite),
				     isClone(clone), effect(NULL), scale(sprite.scale), rot(sprite.rot)
{
//	LOG("Creating clone of CSprite {" << this << "}", 5, LOG_INFO);

	for (map<string,CAnimation*>::const_iterator i = sprite.animations.begin(); i != sprite.animations.end(); ++i)
	{
		animations[i->first] = i->second->getClone();     // construct new map of clones of animations
	}

	currentAnimation = animations.begin()->second;
}

CSprite::CSprite(const CSprite& sprite, bool clone, CVector vel, CVector pos)
				   : CParticle(sprite), isClone(clone), effect(NULL), scale(sprite.scale), rot(sprite.rot)
{
//	LOG("Creating clone of CSprite {" << this << "}", 5, LOG_INFO);

	for (map<string,CAnimation*>::const_iterator i = sprite.animations.begin(); i != sprite.animations.end(); ++i)
	{
		animations[i->first] = i->second->getClone();     // construct new map of clones of animations
	}
	setPos(pos);
	setVelocity(vel);
	timer.reset();
	currentAnimation = sprite.currentAnimation;
}

CSprite::CSprite(CVector v, CVector pos, CSurface* s, const string &animName)
                  : CParticle(v, pos), effect(NULL), scale(1), isClone(false), rot(0)
{
	currentAnimation = addAnimation(animName, new CAnimation(s));
}

CSprite::CSprite(CVector v, CVector p)
         : CParticle(v, p, CRect(0,0,0,0)), currentAnimation(NULL), isClone(false), effect(NULL), scale(1), rot(0)
{
}

CSprite::~CSprite()
{
	if (!isClone)
	{      // not a clone so free up memory
//		LOG("deleting non-clone sprite {" << this << "}", 5, LOG_INFO);

		for (map<string,CAnimation*>::iterator i = animations.begin(); i != animations.end(); ++i)
		{
			delete i->second;
		}
		animations.clear();
	}
	else
	{     // clone, so just do nothing

	}
	delete effect;   // even is this is NULL, we can call delete on it
}

const CSprite& CSprite::operator=(const CSprite &s)
{
	if (&s != this)   // don't allow assignment to self
	{
		// free old memory
		for (map<string,CAnimation*>::iterator i = animations.begin(); i != animations.end(); ++i)
		{
			delete i->second;
		}
		animations.clear();

		// copy over all the CAnimation objects (need to make copies, not just copy pointers)
		for (map<string,CAnimation*>::const_iterator i = s.animations.begin(); i != s.animations.end(); ++i)
		{
			CAnimation* anim = new CAnimation( *i->second );
			animations[i->first] = anim;
			if (s.currentAnimation == i->second)   // if this was the current animation of surf, then make the current animation of this point to the newly allocated object
			{
				currentAnimation = anim;
			}
		}
		velocity = s.velocity;
		pos = s.pos;
		box = s.box;
		moving = s.moving;
		isClone = s.isClone;
		effect = NULL;
		timer = s.timer;
		rot = s.rot;
		scale = s.scale;
	}
	return *this;
}

void CSprite::setAnimation(const string &name)
{
//	LOG("Setting animation of {" << this << "} to '" << name << "'", 5, LOG_INFO);
	if (animations.find(name) == animations.end())
	{
		_THROWEX(ex_illegalArguments, "Animation '" << name << "' not found", "CSprite", "setAnimation", "name = " << name);
		//return;
	}
	currentAnimation = animations[name];
	currentAnimation->reset();
}

void CSprite::setAlpha(Uint32 flags, Uint8 alpha)
{
	if (currentAnimation)
	{
		currentAnimation->setAlpha(flags, alpha);
	}
}

void CSprite::update()
{
	/*for (map<string,CAnimation*>::iterator i = animations.begin(); i != animations.end(); ++i)
	{
		i->second->update();
	}	*/
/*	if (dynamic_cast<CBlock*> (this) != NULL && (dynamic_cast<CBlock*> (this))->getBoardY() < 11)
		cout << "update {" << this << "} [" << velocity << "]" << endl;*/
	if (currentAnimation)
	{
		currentAnimation->update();	
		if (currentAnimation->isDone())
		{
			try
			{
				setAnimation("static");
			}
			catch (ex_illegalArguments e)
			{
				LOG("failed to set animation of {" << this << "} to static", 5, LOG_INFO);
			}
		}

		// Let the CParticle code update position and velocity
		CParticle::update();

		if (box != CRect(0,0,0,0))
		{
			if (pos.getX() + getCurrentFrame()->getWidth() > box.getWidth()+box.getX()) 
			{
				pos.setX( box.getWidth()+box.getX() - getCurrentFrame()->getWidth());
			}
			
			if (pos.getY() + getCurrentFrame()->getHeight() > box.getY() + box.getHeight())
			{	
				pos.setY( box.getHeight()+box.getY() - getCurrentFrame()->getHeight());
			}
		}
	}
}

CAnimation* CSprite::loadAnimation(const string &fileName, const string &name, bool alpha, bool nopath)
{
	CAnimation* tmp = NULL;
	try
	{
		tmp = new CAnimation(fileName, alpha, nopath);
		addAnimation(name, tmp);
	}
	catch (ex_illegalArguments e)
	{
		_ADDTRACEINFO(e, "CSprite", "loadAnimation", "fileName = '" << fileName << "', name = '" << name << "'");
		delete tmp;
		throw e;
	}
	return tmp;
}

CAnimation* CSprite::loadAnimation(const string &fileName, const string &name, int width, int height, int padding, int speed, bool alpha, bool nopath)
{
	CAnimation* tmp = NULL;
	try
	{
		tmp = new CAnimation(fileName, width, height, padding, speed, alpha, nopath);
		addAnimation(name, tmp);
	}
	catch (ex_illegalArguments e)
	{
		_ADDTRACEINFO(e, "CSprite", "loadAnimation", "fileName = '" << fileName << "', name = '" << name << "'" <<
			                                           ", width = " << width << ", height = " << height <<
													   ", padding = " << padding << ", speed = " << speed);
		delete tmp;
		throw e;
	}
	return tmp;
}

void CSprite::draw(CSurface &surface)
{
	if (CSurface *s = getCurrentFrame())
	{
		for (CSequencedEvent::Iter i = events.begin(); i != events.end(); ++i)
		{
			if ((*i)->hasImage())
			{
				s = (*i)->getImage();
			}
			else if ((*i)->hasDraw())
			{
				(*i)->draw(surface, (int)pos.getX(), (int)pos.getY());
				s = NULL;
			}
			if ( !(*i)->isConcurrent() )
			{
				break;
			}
		}

		if (s)
		{
			if (scale != 1 && rot == 0)
			{
				surface.blitScale(*s, CRect(pos.getX(), pos.getY(),0,0), scale );
			}
			else if (rot != 0)
			{
				surface.blitRotScale(*s, CRect(pos.getX(), pos.getY(), 0,0), scale, rot);
			}
			else
			{
				surface.blit(*s, (int)pos.getX(), (int)pos.getY());
			}
			//CGfxPrimitives::drawCircle(pos.getX() + getWidth()/2, pos.getY() + getHeight()/2, 5, CColor(255), surface);
		}
	}
}

CAnimation* CSprite::addAnimation(const string &name, CAnimation* a)
{
	if (!a)
	{
		_THROWEX(ex_illegalArguments, "Attempted to add null pointer as animation", "CSprite", "addAnimation", "name = " << name << ", a = " << a);
	}
	if (isClone)
	{
		_THROWEX(ex_illegalState, "Attempted to add animation to clone of sprite", "CSprite", "addAnimation", "name = " << name << ", a = " << a);
	}

	if( animations.find(name) == animations.end() )
	{     // make sure key doesn't already exist
		return animations[name] = a;
	}
	else
	{
		// delete existing animation
		delete animations[name];
		return animations[name] = a;
	}
}

void CSprite::appendEffect(int duration, CEffect::Effects effect)
{
	switch (effect)
	{
		case CEffect::BLACK:
		{
			//delete CSprite::effect;
			appendEvent( new CBlackEffect(duration, false, *getCurrentFrame()) );
			break;
		}
		default:
		{
			// throw exception
			break;
		}
	}
}

CSurface* CSprite::getCurrentFrame(bool orEffect)
{
	if (orEffect)
	{
		for (CSequencedEvent::Iter i = events.begin(); i != events.end(); ++i)
		{
			if ((*i)->hasImage())
			{
				return (*i)->getImage();
			}
			if (!(*i)->isConcurrent())
				break;
		}
		return (currentAnimation ? currentAnimation->getCurrentFrame() : NULL);
	}
	else
		return (currentAnimation ? currentAnimation->getCurrentFrame() : NULL);
}

CSprite* CSprite::getClone()
{
	CSprite* clone = new CSprite(*this, true);
	return clone;
}

CSprite* CSprite::getClone(CVector vel, CVector pos)
{
	CSprite* clone = new CSprite(*this, true, vel, pos);
	return clone;
}

CParticle::BoundaryInfo CSprite::inBoundaryHoriz(double x)
{
	if (box != CRect(0,0,0,0))
	{
		if (! box.checkLeft(x))
		{
			return OUTSIDE_LOW;
		}
		else if (! box.checkRight(x + getWidth()))
		{
			return OUTSIDE_HIGH;
		}
		else
		{
			return INSIDE;
		}
	}
	else
	{
		return INSIDE;
	}
}

CParticle::BoundaryInfo CSprite::inBoundaryVert(double y)
{
	if (box != CRect(0,0,0,0))
	{
		if (! box.checkBottom(y + getHeight()))
		{
			return OUTSIDE_HIGH;
		}
		else if (! box.checkTop(y))
		{
			return OUTSIDE_LOW;
		}
		else
		{
			return INSIDE;
		}
	}
	else
	{
		return INSIDE;
	}
}

void CSprite::setScale(double factor)
{
	if (scale >= 0 && scale < 10)
		scale = factor;
}

void CSprite::resetScale()
{
	scale = 1;
}

void CSprite::resetRot()
{
	rot = 0;
}

void CSprite::setRot(double rad)
{
	int w = getWidth();
	int h = getHeight();
	double oldRad = rot;

	if (! (rad >= 0 && rad <= 2*PI))
	{
		if (rad < 0)
		{
			while( rad < 0)
			{
				rad += 2*PI;
			}
		}
		else  // rad > 2PI
		{
			while (rad > 2*PI)
			{
				rad -= 2*PI;
			}
		}
		rot=rad;
	}
	rot = rad;
//	cout << rot/PI << "PI rad" << endl;
	//double dR = rot - oldRad;
//	double angle = rot;

/*	if( angle > PI/2 )
	{
		while (angle > PI/2)
		{
			angle -= PI/2;
		}
	}*/
	/*cout << "    " << angle/PI << "PI rad" << endl;
	//dR = dR - PI/2;
	double c = sqrt((double)w*w + (double)h*h);
	double at = acos((double)w/c);
	double comp = (PI/2)-angle;

	double w2 = sin( at + comp) * c;
	double h2 = sin( at + angle ) * c;
	
	cout << "    " << (int)w2 << " " << (int)h2 << endl;

	setPos(50-(w2-2)/2, 50- (h2 - h)/2.);*/
}
