#include "oodle.h"

std::string stringify(double x)
{
	std::ostringstream o;
	if (!(o << x))
		_THROWEX(ex_badConversion, "Bad Conversion Exception", "global", "stringify", "x = " << x);
	return o.str();
}

std::string stringify(int x)
{
	std::ostringstream o;
	if (!(o << x))
		_THROWEX(ex_badConversion, "Bad Conversion Exception", "global", "stringify", "x = " << x);
	return o.str();
}

void log(const std::ostringstream &s, int lvl, logTypes type)
{
	char t;
	if (type == LOG_INFO)
		t = 'I';
	else if (type == LOG_ERROR)
		t = 'E';

#if defined(WIN32) || defined(_WIN32)
	ofstream fout( "log.txt", ios::app );
#else
        char *homedir;
        string logfilename;

        homedir = getenv("HOME");
        if (homedir != NULL)
        {
            logfilename = string(homedir) + "/.blocks-log";
        }
        else
        {
            logfilename = "blocks-log";
        }

        ofstream fout( logfilename.c_str(), ios::app );
#endif

	char tmpbuf[128];
	time_t aclock;
	time( &aclock );   // Get time in seconds
	struct tm *newtime = localtime( &aclock );   // Convert time to struct tm form
	strftime( tmpbuf, 128, "%x %H:%M:%S", newtime );
	fout << tmpbuf << " [" << t << "]: \t" << s.str() << endl;
	/*if (lvl <= 3)
	{*/
		cout << "[" << t << "]\t" << s.str() << endl;
	//}
	fout.close();
}




#if SDL_BYTEORDER == SDL_BIG_ENDIAN
	const Uint32 RMASK = 0xff000000;
	const Uint32 GMASK = 0x00ff0000;
	const Uint32 BMASK = 0x0000ff00;
	const Uint32 AMASK = 0x000000ff;
#else
	const Uint32 RMASK = 0x000000ff;
	const Uint32 GMASK = 0x0000ff00;
	const Uint32 BMASK = 0x00ff0000;
	const Uint32 AMASK = 0xff000000;
#endif
